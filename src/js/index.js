import { getItem, setItem } from './storage';

const todos = getItem('todos') || [];
    
function renderList() {
  const todoList = document.getElementById('todos');
  const completedList = document.getElementById('completed');
  todoList.innerHTML = '';
  completedList.innerHTML = '';
  todos.forEach((todo) => {
    const list = document.createElement('li');
    list.setAttribute('data-val', todo.id);
    list.innerText = todo.todo;
    if (todo.isCompleted) {
      completedList.appendChild(list);
    } else {
      list.addEventListener('click', completeTodo);
      todoList.appendChild(list);
    }
  });
}
    
window.addEventListener('load', renderList);
    
function completeTodo(e) {
  const currentId = e.target.getAttribute('data-val');
  const todo = todos.find((item) => item.id === currentId);
  todo.isCompleted = true;
  renderList();
  setItem('todos', todos);
}

const form = document.getElementById('form');
form.addEventListener('submit', (e) => {
  e.preventDefault();
  const input = document.getElementById('input');
  const todo = input.value;
  input.value = '';
  todos.push({
    id: Date.now() + todo,
    todo: todo,
    isCompleted: false,
  });
  setItem('todos', todos);
  renderList();
});
   