export const getItem = (key) => {
  const result = localStorage.getItem(key);
  return JSON.parse(result);
};

export const setItem = (key, value) => {
  let flag = true;
  try {
    localStorage.setItem(key, JSON.stringify(value));
    throw false;
  } catch(e) {
    flag = e;
  }

  return flag;
}
