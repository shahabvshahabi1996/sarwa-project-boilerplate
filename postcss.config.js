const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = {
  plugins: [
    'postcss-preset-env',
    require('tailwindcss'),
    process.env.NODE_ENV === 'production' ? require('autoprefixer') : null,
    purgecss({
      content: ['./src/index.html'],
      defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
    })
  ],
};